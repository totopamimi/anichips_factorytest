package anichips.videotest;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by totopamimi on 17/4/24.
 */

public class MyBluetoothAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<String> mList;

    public MyBluetoothAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    public ArrayList<String> getList() {
        return mList;
    }

    public void setList(ArrayList<String> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String str = "正在搜索蓝牙设备...";
        TextView tv = new TextView(mContext);
        tv.setTextSize(16);
        tv.setTextColor(Color.BLACK);
        tv.setText(mList.size() == 0 ? str : mList.get(position));
        return tv;
    }
}
