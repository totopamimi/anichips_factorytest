package anichips.videotest;

import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String SU_NAME = "anichips";
    private static final int MESSAGE_WIFI_REACH = 1;
    private static final int MESSAGE_WIFI_UNREACH = 0;
    private final MyHandler mHandler = new MyHandler(this);
    private Button mUSB1Button;
    private Button mUSB2Button;
    private String mUSB1Name;
    private String mUSB2Name;
    private Context mContext;
    private TextView mMemoryText;
    private SimpleDateFormat mVideoPlayFormat;
    private UsbManager mUsbManager;
    private final Runnable mUSBDeviceRunnable = new Runnable() {
        @Override
        public void run() {
            mUsbManager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
            HashMap<String, UsbDevice> usbDeviceList = mUsbManager.getDeviceList();
            Iterator<UsbDevice> deviceIterator = usbDeviceList.values().iterator();
            while (deviceIterator.hasNext()) {
                UsbDevice device = deviceIterator.next();
                String deviceName = device.getDeviceName();
                Log.d(TAG, "getDeviceName=" + deviceName);
                if (!deviceName.isEmpty() && mUSB1Name == null) {
                    mUSB1Name = deviceName;
                    setButtonColor(mUSB1Button, true);
                } else if (!deviceName.isEmpty() && mUSB1Name != null && !deviceName.equals(mUSB1Name)) {
                    mUSB2Name = deviceName;
                    setButtonColor(mUSB2Button, true);
                }
                // ToastUtils.makeText(mContext, "device.getDeviceName()=" + device.getDeviceName(), Toast.LENGTH_LONG).show();
            }
            mHandler.postDelayed(this, 3000);
        }
    };
    private PowerManager mPowerManager;
    private ConnectivityManager mConnectivityManager;
    private ActivityManager mActivityManager;
    private ActivityManager.MemoryInfo mMemoryInfo;
    private final Runnable mMemoryRunnable = new Runnable() {
        @Override
        public void run() {
            if (mMemoryInfo == null) {
                mMemoryInfo = new ActivityManager.MemoryInfo();
            }
            mActivityManager.getMemoryInfo(mMemoryInfo);
            mMemoryText.setText((mMemoryInfo.availMem / 1024 / 1024) + "M(剩余)/" + (mMemoryInfo.totalMem / 1024 / 1024) + "M(总)");
            mHandler.postDelayed(this, 3000);
        }
    };
    private long beginTime = 0;
    private boolean isWifiWeak;
    private boolean isEthernetConnected;
    private BluetoothAdapter mBluetoothAdapter;
    private final Runnable mBluetoothRunnable = new Runnable() {
        @Override
        public void run() {
            if (!mBluetoothAdapter.isEnabled()) {
                mBluetoothAdapter.enable();
            } else {
                if (!mBluetoothAdapter.isDiscovering()) {
                    mBluetoothAdapter.startDiscovery();
                }
            }
            mHandler.postDelayed(this, 3000);
        }
    };
    private WifiManager mWifiManager;
    private List<android.net.wifi.ScanResult> mWifiScanResult;
    private Button mWifiButton;
    private Button mBluetoothButton;
    /**
     * 权限：
     * <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
     * <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>
     */
    private final BroadcastReceiver mBluetoothFoundReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress();
                // ToastUtils.makeText(mContext, deviceName + "|" + deviceHardwareAddress, Toast.LENGTH_LONG).show();
                Log.d(TAG, "bluetooth deviceName=" + deviceName + ", deviceHardwareAddress=" + deviceHardwareAddress);
                setButtonColor(mBluetoothButton, true);
            }
        }
    };
    private Button mSDCardButton;
    private File mFile;
    private final Runnable mSDCardRunnable = new Runnable() {
        @Override
        public void run() {
            List sdcardList = SDCardUtils.getExtSDCardPath(mContext);
            if (sdcardList.size() >= 2) {
                for (int i = 0; i < sdcardList.size(); i++) {
                    String path = sdcardList.get(i).toString();
                    if (!path.equals(Environment.getExternalStorageDirectory().getPath()) && path.contains("/storage/")) {
                        mFile = new File(path + File.separator);
                        // ToastUtils.makeText(mContext, file.getPath(), Toast.LENGTH_SHORT).show();
                    }
                }
            } else if (sdcardList.size() == 1) {
                if (sdcardList.get(0).toString().equals(Environment.getExternalStorageDirectory().getPath())) {
                }
            }

            if (mFile != null && mFile.canRead() && mFile.canWrite()) {
                setButtonColor(mSDCardButton, true);
            } else {
                setButtonColor(mSDCardButton, false);
            }
            // ToastUtils.makeText(mContext, "" + SDCardUtils.getExtSDCardPath(mContext).toString(), Toast.LENGTH_SHORT).show();
            mHandler.postDelayed(this, 3000);
        }
    };
    private File mAnichipsFile;
    private final Runnable mWifiScanRunnable = new Runnable() {
        @Override
        public void run() {
            if (!mWifiManager.isWifiEnabled()) {
                if (!isEthernetConnected) {
                    mWifiManager.setWifiEnabled(true);
                } else {
                    setButtonColor(mWifiButton, false);
                }
            } else {
                if (mWifiManager.startScan()) {
                    mWifiScanResult = mWifiManager.getScanResults();
                    Log.d(TAG, "mWifiSize=" + mWifiScanResult.size());
                    // open gps
                    if (!mWifiScanResult.isEmpty()) {
                        if (!mAnichipsFile.exists()) {
                            mWifiScanResult = null;
                        }
                        int max = Integer.MIN_VALUE;
                        for (int i = 0; i < mWifiScanResult.size(); i++) {
                            int tmp = mWifiScanResult.get(i).level;
                            if (tmp >= max) {
                                max = tmp;
                            }
                        }
                        Log.d(TAG, "Wifi max=" + max);
                        if (max < -60) {
                            // setWeakColor(mWifiButton);
                            isWifiWeak = true;
                        } else {
                            // setButtonColor(mWifiButton, true);
                            isWifiWeak = false;
                        }
                    } else {
                        // setWeakColor(mWifiButton);
                        isWifiWeak = true;
                    }
                } else {
                    setButtonColor(mWifiButton, false);
                }
                testWifiReachable(mContext);
            }
            mHandler.postDelayed(this, 3000);
        }
    };
    private File mRTCFile;
    private Button mIJ45Button;
    private final Runnable mEthernetRunnable = new Runnable() {
        @Override
        public void run() {
            NetworkInfo activeNetwork = mConnectivityManager.getActiveNetworkInfo();
            if (activeNetwork != null) {
                // connected to the internet
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    // connected to wifi
                    Log.d(TAG, "connected to wifi");
                } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                    // connected to the mobile provider's data plan
                    Log.d(TAG, "connected to the mobile provider's data plan");
                } else if (activeNetwork.getType() == ConnectivityManager.TYPE_ETHERNET) {
                    // connected to ethernet
                    Log.d(TAG, "connected to ethernet");
                    setButtonColor(mIJ45Button, true);
                    isEthernetConnected = true;
                    mWifiManager.setWifiEnabled(false);
                }
            } else {
                // not connected to the internet
            }
            mHandler.postDelayed(this, 3000);
        }
    };
    private final BroadcastReceiver mEthernetBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.net.ethernet.ETHERNET_STATE_CHANGED")) {
                if (intent.getIntExtra("ethernet_state", 0/*ETHER_STATE_DISCONNECTED*/) == 2/*ETHER_STATE_CONNECTED*/) {
                    Log.d(TAG, "ethernet connected");
                    setButtonColor(mIJ45Button, true);
                    isEthernetConnected = true;
                    mWifiManager.setWifiEnabled(false);

                } else if (intent.getIntExtra("ethernet_state", 0/*ETHER_STATE_DISCONNECTED*/) == 0/*ETHER_STATE_DISCONNECTED*/) {
                    Log.d(TAG, "ethernet disconnected");
                    setButtonColor(mIJ45Button, false);
                    isEthernetConnected = false;
                }
            }
        }
    };
    private Button mRTCButton;
    private TextView mRTCText;
    private TextView mVideoPlayText;
    private SimpleDateFormat mRTCFormat;
    private MediaPlayer.OnErrorListener mMediaPlayerErrorListener = new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle("视频测试结果");
            builder.setMessage("失败！");
            builder.setPositiveButton(android.R.string.ok, null);
            builder.show();
            return false;
        }
    };
    private MediaPlayer mMediaPlayer;
    private final Runnable mRTCRunnable = new Runnable() {
        @Override
        public void run() {
            if (mMediaPlayer == null && beginTime != 0 && getCurrentTime() - beginTime > 365 * 24 * 60 * 60 * 1000) {
                mVideoPlayText.setText("视频测试成功!");
                mVideoPlayText.setTextColor(Color.GREEN);
            } else if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                mVideoPlayText.setText(mVideoPlayFormat.format(getCurrentTime() - beginTime - TimeZone.getDefault().getRawOffset()));
                mVideoPlayText.setTextColor(ContextCompat.getColor(mContext, R.color.button_color));
            } else if (mMediaPlayer != null && !mMediaPlayer.isPlaying()) {
                mVideoPlayText.setText("视频播放异常");
                mVideoPlayText.setTextColor(Color.RED);
                // Log.d(TAG, "mMediaPlayer.getCurrentPosition()=" + mMediaPlayer.getCurrentPosition());
            }
            if (!mRTCFile.exists()) {
                mRTCText.setText("RTC异常");
                mRTCText.setTextColor(Color.RED);
                setButtonColor(mRTCButton, false);
            } else {
                mRTCText.setText(mRTCFormat.format(System.currentTimeMillis()));
                mRTCText.setTextColor(ContextCompat.getColor(mContext, R.color.button_color));
                setButtonColor(mRTCButton, true);
            }
            mHandler.postDelayed(this, 3000 / 3);
        }
    };
    private final BroadcastReceiver mScreenOffBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_SCREEN_ON)) {
                if (mMediaPlayer != null) {
                    mMediaPlayer.start();
                }
            } else if (action.equals(Intent.ACTION_SCREEN_OFF)) {
                if (mMediaPlayer != null) {
                    mMediaPlayer.pause();
                }
            }
        }
    };
    private SurfaceHolder.Callback mSurfaceHolderCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            if (mMediaPlayer != null) {
                mMediaPlayer.setDisplay(holder);
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            if (mMediaPlayer != null) {
                mMediaPlayer.setDisplay(null);
            }
        }
    };
    private SurfaceView mSurfaceView;
    private SurfaceHolder mSurfaceHolder;
    // private MyVideoView mMyVideoView;
    private MediaPlayer.OnPreparedListener mMediaPlayerPreparedListener = new MediaPlayer.OnPreparedListener() {
        @Override
        public void onPrepared(MediaPlayer mp) {
            clearSurfaceViewBackground();
            // mp.setLooping(true);
            mp.start();
            // first time flag
            if (beginTime == 0) {
                beginTime = getCurrentTime();
            } else {
                mp.setDisplay(mSurfaceHolder);
            }
            // mp.seekTo(200000);
        }
    };
    private MediaPlayer.OnCompletionListener mMediaPlayerCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            long diff = getCurrentTime() - beginTime;
            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);
            Log.d(TAG, "Days=" + diffDays + ", Hours=" + diffHours + ", Minutes=" + diffMinutes + ", diffSeconds=" + diffSeconds + ", diff=" + diff);
            if (diff >= 365 * 24 * 60 * 60 * 1000 && beginTime != 0) {
                mp.stop();
                mp.setDisplay(null);
                mp.reset();
                mp.release();
                mMediaPlayer = null;
                if (!mAnichipsFile.exists()) {
                    mSurfaceView = null;
                }
                mSurfaceView.setBackgroundResource(R.drawable.video_bg);
                // mMyVideoView.setBackgroundResource(R.drawable.video_bg);
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("视频测试结果");
                builder.setMessage("成功！");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.show();
            } else {
                clearSurfaceViewBackground();
                mp.stop();
                mp.setDisplay(null);
                mp.reset();
                mp.release();
                mSurfaceHolder.removeCallback(mSurfaceHolderCallback);
                mSurfaceView = null;
                // mMediaPlayer = null;
                initVideo();
                // mp.release();
                // mVideoPlayText.setText(mVideoPlayFormat.format(diff - TimeZone.getDefault().getRawOffset()));
            }
        }
    };

    private long getCurrentTime() {
        return new Date(System.currentTimeMillis()).getTime();
    }

    private String md5(String string) {
        byte[] hash;
        try {
            hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Huh, MD5 should be supported?", e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Huh, UTF-8 should be supported?", e);
        }

        StringBuilder hex = new StringBuilder(hash.length * 2);
        for (byte b : hash) {
            if ((b & 0xFF) < 0x10)
                hex.append("0");
            hex.append(Integer.toHexString(b & 0xFF));
        }
        return hex.toString();
    }

    private int getSignature(Context context) {
        PackageManager pm = context.getPackageManager();
        PackageInfo pi;
        StringBuilder sb = new StringBuilder();
        try {
            pi = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            Signature[] signatures = pi.signatures;
            for (Signature signature : signatures) {
                sb.append(signature.toCharsString());
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return sb.toString().hashCode();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        mAnichipsFile = new File("/system/xbin/anichips");
        // if (!getString(Integer.toHexString(16776975)).equals(md5(getSignature(mContext) + ""))) {
        //     return;
        // }
        if (!mAnichipsFile.exists()) {
            return;
        }

        init();
        initWifi();
        initBluetooth();
        initSDCard();
        initIJ45();
        initRTC();
        initUSB1();

        initVideo();
        initMemory();
        DisplayMetrics dm = new DisplayMetrics();
        ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(dm);
        Log.d(TAG, "screenWidth=" + dm.widthPixels + ", screenHeight=" + dm.heightPixels + ", density=" + getResources().getDisplayMetrics().density + ", dpi=" + getResources().getDisplayMetrics().densityDpi);
    }

    private void initMemory() {
        mActivityManager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        mMemoryText = (TextView) findViewById(R.id.my_memory);
        mHandler.postDelayed(mMemoryRunnable, 3000);
    }

    private String getString(String string) {
        return "740e5c4d9cc2" + string + "371ef533dc4b65";
    }

    private void init() {
        mRTCText = (TextView) findViewById(R.id.rtc_time_value);
        mVideoPlayText = (TextView) findViewById(R.id.video_time_title_value);
        mRTCFormat = new SimpleDateFormat("yyyy-MM-dd    HH:mm:ss", Locale.getDefault());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        mContext.registerReceiver(mScreenOffBroadcastReceiver, intentFilter);
    }

    private void initWifi() {
        mWifiButton = (Button) findViewById(R.id.my_wifi);
        mWifiManager = (WifiManager) mContext.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        boolean isWifiOn = mWifiManager.isWifiEnabled();
        boolean isWifiWork = true;
        if (isWifiOn) {
            // setButtonColor(mWifiButton, true);
        } else {
            if (isWifiWork = mWifiManager.setWifiEnabled(true)) {
                // setButtonColor(mWifiButton, true);
            } else {
                setButtonColor(mWifiButton, false);
            }
        }
        if (!isWifiWork) {
            return;
        }
        mHandler.postDelayed(mWifiScanRunnable, 3000);
    }

    private void initBluetooth() {
        mBluetoothButton = (Button) findViewById(R.id.my_bluetooth);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            setButtonColor(mBluetoothButton, false);
            return;
        }
        // if (!getString(Integer.toHexString(16776975)).equals(md5(getSignature(mContext) + ""))) {
        //     mBluetoothAdapter = null;
        // }
        // if (!getString(Integer.toHexString(16776975)).equals(md5(getSignature(mContext) + ""))) {
        //     new Thread(new Runnable() {
        //         @Override
        //         public void run() {
        //             while (true) {
        //                 Toast.makeText(mContext, ".", Toast.LENGTH_LONG).show();
        //             }
        //         }
        //     }).start();
        // }
        boolean isBluetoothOn = mBluetoothAdapter.isEnabled();
        boolean isBluetoothWork = true;
        if (isBluetoothOn) {
            setButtonColor(mBluetoothButton, true);
        } else {
            if (isBluetoothWork = mBluetoothAdapter.enable()) {
                setButtonColor(mBluetoothButton, true);
            } else {
                setButtonColor(mBluetoothButton, false);
            }
        }
        if (!isBluetoothWork) {
            return;
        }
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mBluetoothFoundReceiver, filter);

        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 3600);
        startActivity(discoverableIntent);

        mHandler.postDelayed(mBluetoothRunnable, 3000);
    }

    private void initSDCard() {
        // mFile = new File("/storage/0C0D-0917/");
        // mFile = new File(Environment.getExternalStorageDirectory().getPath() + File.separator);
        mSDCardButton = (Button) findViewById(R.id.my_sdcard);
        mHandler.postDelayed(mSDCardRunnable, 3000);
    }

    private void initIJ45() {
        mIJ45Button = (Button) findViewById(R.id.my_rj45);
        mConnectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        mContext.registerReceiver(mEthernetBroadcastReceiver, new IntentFilter("android.net.ethernet.ETHERNET_STATE_CHANGED"));
        mHandler.postDelayed(mEthernetRunnable, 3000);
    }

    private void initRTC() {
        mRTCButton = (Button) findViewById(R.id.my_rtc);
        mRTCFile = new File("/dev/rtc0");
        mHandler.postDelayed(mRTCRunnable, 3000 / 3);
    }

    private void initUSB1() {
        mUSB1Button = (Button) findViewById(R.id.my_usb1);
        mUSB2Button = (Button) findViewById(R.id.my_usb2);
        mHandler.postDelayed(mUSBDeviceRunnable, 3000);
    }

    private void initVideo() {
        mVideoPlayFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        // mMyVideoView = (MyVideoView) findViewById(R.id.my_videoview);
        mSurfaceView = (SurfaceView) findViewById(R.id.my_surfaceview);
        // Uri videoUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory().getPath() + File.separator + "factory.mp4"));
        File MP4_VIDEO = new File("/system/media/video/factory.mp4");
        File MKV_VIDEO = new File("/system/media/video/factory.mkv");
        Uri videoUri = null;
        if (MP4_VIDEO.exists()) {
            videoUri = Uri.fromFile(MP4_VIDEO);
        } else if (MKV_VIDEO.exists()) {
            videoUri = Uri.fromFile(MKV_VIDEO);
        }
        if (videoUri == null) {
            return;
        }
        // mMyVideoView.setVideoURI(videoUri);
        if (mSurfaceHolder == null) {
            mSurfaceHolder = mSurfaceView.getHolder();
        }
        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(mContext, videoUri, null);
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.setOnPreparedListener(mMediaPlayerPreparedListener);
            mMediaPlayer.setOnCompletionListener(mMediaPlayerCompletionListener);
            mMediaPlayer.setOnErrorListener(mMediaPlayerErrorListener);
            mMediaPlayer.prepareAsync();
            mMediaPlayer.setScreenOnWhilePlaying(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mSurfaceHolder.setKeepScreenOn(true);
        mSurfaceHolder.addCallback(mSurfaceHolderCallback);
        // mMediaController = new MediaController(mContext);
        // mMyVideoView.setMediaController(mMediaController);
        // mMyVideoView.setKeepScreenOn(true);
        // mMyVideoView.setOnPreparedListener(mMediaPlayerPreparedListener);
        // mMyVideoView.setOnCompletionListener(mMediaPlayerCompletionListener);
        // mMyVideoView.setOnErrorListener(mMediaPlayerErrorListener);
    }

    private void clearSurfaceViewBackground() {
        // if (mMyVideoView.getBackground() != null) {
        //     mMyVideoView.setBackground(null);
        // }
        if (mSurfaceView != null && mSurfaceView.getBackground() != null) {
            mSurfaceView.setBackground(null);
        }
    }

    private void testWifiReachable(Context context) {
        /*
        //  First, check we have connectivity
        final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null && ni.getType() == ConnectivityManager.TYPE_WIFI && ni.isConnected()) {
            //  check if g.cn is reachable
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        URL url = new URL("https://www.baidu.com");
                        HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                        urlc.setRequestProperty("Connection", "close");
                        urlc.setConnectTimeout(10 * 1000); // Ten seconds timeout in milliseconds
                        urlc.connect();
                        if (urlc.getResponseCode() == 200) { // success
                            // return true;
                            mHandler.sendEmptyMessage(MESSAGE_WIFI_REACH);
                        } else { // Fail
                            // return false;
                            mHandler.sendEmptyMessage(MESSAGE_WIFI_UNREACH);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        // return false;
                        mHandler.sendEmptyMessage(MESSAGE_WIFI_UNREACH);
                    }
                }
            }).start();
        }
        */

        NetworkInfo activeNetwork = mConnectivityManager.getActiveNetworkInfo();
        if (activeNetwork != null) {
            // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                Log.d(TAG, "connected to wifi");
                mHandler.sendEmptyMessage(MESSAGE_WIFI_REACH);
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                Log.d(TAG, "connected to the mobile provider's data plan");
                mHandler.sendEmptyMessage(MESSAGE_WIFI_UNREACH);
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_ETHERNET) {
                // connected to ethernet
                Log.d(TAG, "connected to ethernet");
                mHandler.sendEmptyMessage(MESSAGE_WIFI_UNREACH);
            }
        } else {
            // not connected to the internet
            mHandler.sendEmptyMessage(MESSAGE_WIFI_UNREACH);
        }
    }

    private void setButtonColor(Button button, boolean on) {
        button.setBackgroundResource(on ? R.drawable.success_bg : R.drawable.error_bg);
    }

    private void setWeakColor(Button button) {
        button.setBackgroundResource(R.drawable.weak_bg);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // if (mMyVideoView != null) {
        //     mMyVideoView.stopPlayback();
        //     mMyVideoView.setKeepScreenOn(false);
        //     mMyVideoView = null;
        // }
        // if (mMediaController != null) {
        //     mMediaController = null;
        // }
        if (mSurfaceHolder != null) {
            mSurfaceHolder.setKeepScreenOn(false);
            mSurfaceHolder.removeCallback(mSurfaceHolderCallback);
        }
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
        mHandler.removeCallbacks(mWifiScanRunnable);
        mHandler.removeCallbacks(mBluetoothRunnable);
        mHandler.removeCallbacks(mSDCardRunnable);
        mHandler.removeCallbacks(mRTCRunnable);
        mHandler.removeCallbacks(mEthernetRunnable);
        mHandler.removeCallbacks(mMemoryRunnable);
        mHandler.removeCallbacksAndMessages(null);
        unregisterReceiver(mBluetoothFoundReceiver);
        unregisterReceiver(mEthernetBroadcastReceiver);
        unregisterReceiver(mScreenOffBroadcastReceiver);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        return super.onKeyLongPress(keyCode, event);
    }

    @Override
    public boolean onKeyMultiple(int keyCode, int repeatCount, KeyEvent event) {
        return super.onKeyMultiple(keyCode, repeatCount, event);
    }

    @Override
    public boolean onKeyShortcut(int keyCode, KeyEvent event) {
        return super.onKeyShortcut(keyCode, event);
    }

    private static class MyHandler extends Handler {
        private final WeakReference<MainActivity> mActivity;

        private MyHandler(MainActivity activity) {
            mActivity = new WeakReference<MainActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            MainActivity activity = mActivity.get();
            if (activity != null) {
                switch (msg.what) {
                    case MESSAGE_WIFI_REACH:
                        if (activity.isWifiWeak) {
                            activity.setWeakColor(activity.mWifiButton);
                        } else {
                            activity.setButtonColor(activity.mWifiButton, true);
                        }
                        break;
                    case MESSAGE_WIFI_UNREACH:
                        activity.setButtonColor(activity.mWifiButton, false);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
