package anichips.videotest;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by totopamimi on 17/4/24.
 */

public class MySDCardAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<String> mList;
    private String mSDCardPath;

    public MySDCardAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    public void setSDCardPath(String SDCardPath) {
        mSDCardPath = SDCardPath;
    }

    public List<String> getList() {
        return mList;
    }

    public void setList(List<String> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String str = "SDCard内没有文件";
        TextView tv = new TextView(mContext);
        tv.setTextSize(16);
        tv.setTextColor(Color.BLACK);
        tv.setText(mList.size() == 0 ? str : mSDCardPath + mList.get(position));
        return tv;
    }
}
