package anichips.videotest;

import android.content.Context;
import android.graphics.Color;
import android.net.wifi.ScanResult;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by totopamimi on 17/4/24.
 */

public class MyWifiAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<ScanResult> mScanResults;

    public MyWifiAdapter(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
    }

    public List<ScanResult> getScanResults() {
        return mScanResults;
    }

    public void setScanResults(List<ScanResult> scanResults) {
        mScanResults = scanResults;
    }

    @Override
    public int getCount() {
        return mScanResults.size();
    }

    @Override
    public Object getItem(int position) {
        return mScanResults.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String str = "正在搜索WIFI设备...";
        TextView tv = new TextView(mContext);
        tv.setTextSize(16);
        tv.setTextColor(Color.BLACK);
        tv.setText(mScanResults.size() == 0 ? str : "WIFI设备地址：" + mScanResults.get(position).SSID);
        return tv;
    }
}
