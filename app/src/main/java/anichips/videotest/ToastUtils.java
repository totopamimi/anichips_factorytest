package anichips.videotest;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by totopamimi on 17/3/17.
 */

public class ToastUtils {
    private static Toast sToast;

    public static Toast makeText(Context context, CharSequence text, int duration) {
        if (sToast == null) {
            sToast = Toast.makeText(context.getApplicationContext(), text, duration);
        } else {
            sToast.setText(text);
        }
        return sToast;
    }
}
